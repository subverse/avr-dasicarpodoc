#ifndef __DASICARPODOC_H__
#define __DASICARPODOC_H__

#define AMP_PORT                    PORTD
#define AMP_PIN                     PD7
#define AMP_OFF()                   (AMP_PORT&=~_BV(AMP_PIN))
#define AMP_ON()                    (AMP_PORT|=_BV(AMP_PIN))

#define IIC_BUTTONS                 IIC_ADDRESS_0
#define RAW_BUTTON_NONE             0xFF
#define RAW_BUTTON_ERROR            0x7F
#define RAW_BUTTON_RELEASE          0x3F

#define RAW_BUTTON_PLAY_PAUSE       0xFB
#define RAW_BUTTON_VOLUME_UP        0xFE
#define RAW_BUTTON_VOLUME_DOWN      0xEF
#define RAW_BUTTON_SKIP_FORWARD     0xBF
#define RAW_BUTTON_SKIP_BACKWARD    0xFA
#define RAW_BUTTON_MENU             0xDF

#define BUTTON_NONE                 0x00
#define BUTTON_PLAY_PAUSE           0x01
#define BUTTON_VOLUME_UP            0x02
#define BUTTON_VOLUME_DOWN          0x03
#define BUTTON_SKIP_FORWARD         0x04
#define BUTTON_SKIP_BACKWARD        0x05
#define BUTTON_MENU                 0x06
#define BUTTON_RELEASE              0x07
#define BUTTON_ERROR                0x08
#define BUTTON_TIMEOUT              0x09
#define BUTTON_PLAYLIST_NEXT        0x0A
#define BUTTON_PLAYLIST_PREV        0x0B

#define PLAY_SYMBOL                 1
#define PAUSE_SYMBOL                2

#define HOUR_ONES                   3600000
#define MINUTE_TENS                 600000
#define MINUTE_ONES                 60000
#define SECOND_TENS                 10000
#define SECOND_ONES                 1000

uint8_t msec2ascii(char *, uint32_t);
uint8_t getButton(uint8_t, uint16_t);
void inithardware(void);
uint8_t goToSleep(void);

#endif
