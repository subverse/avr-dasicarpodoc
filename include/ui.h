#ifndef __UI_H__
#define __UI_H__

uint8_t ui_init(void);
void ui_show_welcome(void);
uint8_t ui_update_display(ipod_t *);
uint8_t ui_show_volume(void);
void ui_show_error(void);

#endif /*UI_H_*/
