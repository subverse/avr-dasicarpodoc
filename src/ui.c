///
///\file   ui.c
///
///\brief   User Interface for iPod project
///
///\author   <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version   1.1 10/10/2010
///         - formatted for doxygen
///
///\version   1.0
///
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include "dasicarpodoc.h"
#include "iiccharlcd.h"
#include "bargraph.h"
#include "stv5730.h"
#include "ds1807.h"
#include "ipod.h"
#include "ui.h"

static void ui_show_message(const char *, uint8_t, uint8_t, uint8_t tv_row);

//bitpatterns for play and pause symbol for LCD display
static const char symbolPlay[] PROGMEM = {0x00, 0x08, 0x0C, 0x0E, 0x0C, 0x08, 0x00, 0x00};
static const char symbolPause[] PROGMEM = {0x00, 0x0A, 0x0A, 0x0A, 0x0A, 0x0A, 0x00, 0x00};

static char lcdBuffer[40];

///
///\brief Initialise user interface
///
uint8_t ui_init(void)
{
   stv_reset(STV_PAL);                        //reset stv5730 video driver
   lcd_open(LCD_4_BIT_SML, 16, 2);
   lcd_switch_display(LCD_DISPLAY_ONE);
   lcd_set_CG_RAM_P(LCD_CG_RAM_SIX, symbolPlay);                        //load play symbol into LCD CG RAM
   lcd_set_CG_RAM_P(LCD_CG_RAM_SEVEN, symbolPause);                        //load pause symbol into LCD CG RAM
   bar_init(5, 150);
   return 1;
}

///
///\brief Display welcome message
///
void ui_show_welcome(void)
{
   ui_show_message(PSTR("DAVE'S iPod DOCK"), LCD_DISPLAY_ONE, LCD_LINE_TWO, STV_ROW_FOUR);
   ui_show_message(PSTR("DOCK EMPTY"), LCD_DISPLAY_TWO, LCD_LINE_ONE, STV_ROW_SIX);
}

///
///\brief Display error message
///
void ui_show_error(void)
{
   ui_show_message(PSTR("iPod INITIALISATION FAILED"), LCD_DISPLAY_ONE, LCD_LINE_TWO, STV_ROW_FOUR);
   ui_show_message(PSTR("REMOVE AND RE-INSERT iPod!"), LCD_DISPLAY_TWO, LCD_LINE_ONE, STV_ROW_SIX);
}

///
///\brief Display volume on LCD as bar graph
///
uint8_t ui_show_volume(void)
{
   uint8_t channel;
   uint16_t volume;

   //bargraph is 150 (30 *5) single segments wide
   ds_get_volume(&volume);
   channel = ((uint8_t) (volume & 0x003F)) << 1;
   if (channel < 127)
      channel = 126 - channel;

   bar_write(channel, LCD_LINE_ONE + 5);

   return 1;
}

///
///\brief Update LCD display with artist/track and time info.
///
uint8_t ui_update_display(ipod_t *ipod)
{
   uint16_t tmpPosition;                              //current position in playlist

   ipod_ioctl(ipod, IPOD_GET_TIME_STATUS);               //get track time and play state

   lcd_switch_display(LCD_DISPLAY_ONE);
   //lcd_blank_line( LCD_LINE_ONE );
   lcd_ioctl(LCD_LINE_ONE);
   lcd_write(ipod->status + 5);                        //display play or pause symbol
   msec2ascii(ipod->trackTimeS.total, ipod->trackTime.total);
   msec2ascii(ipod->trackTimeS.elapsed, ipod->trackTime.elapsed);

   sprintf(lcdBuffer, "%5d of %5d     %s  %s", ipod->song.current, ipod->song.total, ipod->trackTimeS.total,
         ipod->trackTimeS.elapsed);
   lcd_print(LCD_LINE_ONE + 1, lcdBuffer);

   stv_set_position(STV_ROW_ONE, 0);
   if (ipod->status == STATUS_PLAYING)
      stv_write("]", 1);
   else
      stv_write("[", 1);
   stv_set_position(STV_ROW_ONE, 10);
   stv_write(lcdBuffer, 15);                           //write song numbers
   stv_set_position(STV_ROW_ELEVEN, 0);
   stv_write(lcdBuffer + 28, 5);                        //total track time
   stv_set_position(STV_ROW_ELEVEN, 22);
   stv_write(lcdBuffer + 34, 5);                        //elapsed track time

   tmpPosition = ipod->song.current;
   ipod_ioctl(ipod, IPOD_GET_CURRENT_POSITION);            //get current position in playlist
   if (tmpPosition != ipod->song.current)               //not same as currently displayed info
   {
      ipod_ioctl(ipod, IPOD_GET_ARTIST_OF);
      ipod_ioctl(ipod, IPOD_GET_TITLE_OF);
      ipod_ioctl(ipod, IPOD_GET_ALBUM_OF);
      ipod_ioctl(ipod, IPOD_COUNT_SONGS);               //number of songs in current playlist

      lcd_blank_line( LCD_LINE_TWO);
      lcd_print_centered( LCD_LINE_TWO, ipod->artist);

      lcd_switch_display( LCD_DISPLAY_TWO);

      lcd_ioctl( LCD_CLEAR_SCREEN);
      lcd_print_centered( LCD_LINE_ONE, "  ");
      lcd_print_centered( LCD_LINE_ONE, ipod->title);
      lcd_print_centered( LCD_LINE_TWO, ipod->album);

      stv_blank_line( STV_ROW_FOUR);
      stv_write_centered(ipod->artist, STV_ROW_FOUR);
      stv_blank_line( STV_ROW_SIX);
      stv_write_centered(ipod->title, STV_ROW_SIX);
      stv_blank_line( STV_ROW_EIGHT);
      stv_write_centered(ipod->album, STV_ROW_EIGHT);

   }
   return 1;
}

///
///\brief Display message from program memory
///
///\param message message to display
///
///\param display which LCD display to show message on
///\arg \c LCD_DISPLAY_ONE
///\arg \c LCD_DISPLAY_TWO
///
///\param position where on the LCD to show the message
///\param TV row to display message
///
static void ui_show_message(const char * message, uint8_t display, uint8_t position, uint8_t tv_row)
{
   stv_clear_screen();
   lcd_switch_display(display);
   lcd_ioctl(LCD_CLEAR_SCREEN);
   sprintf_P(lcdBuffer, message);
   lcd_print_centered(position, "  ");
   lcd_print_centered(position, lcdBuffer);
   stv_write_centered(lcdBuffer, tv_row);
}
