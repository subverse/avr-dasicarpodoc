///
///\file      dasicarpodoc.c
///
///\brief      DASICARPODOC DAve's SImple CAR iPOd DOCk
///
///\details      iPod dock, Advanced iPod Remote (AiR) device
///
///\author      <A HREF="http://www.subverse.co.nz">David McKelvie</A>
///
///\version      1.1 10/10/2010
///            - formatted for doxygen
///
///\version      1.0
///
///\note      see www.subverse.co.nz/dasicarpodoc.html for a description
///            of the hardware used for this project. Basically it is:
///            - an ATMega32
///            - a character LCD display connected via two PCF8574 IO ports
///            - some push button switches connected via another PCF8574
///            - an iPod dock connector
///
///\verbatim
///PORT MAP:
///     A0  X       B0  X       C0  CL      D0  RX
///     A1  X       B1  X       C1  SDA     D1  TX
///     A2  X       B2  X       C2  TCK     D2  X
///     A3  X       B3  X       C3  TMS     D3  INT1 - iPod detect
///     A4  X       B4  SS      C4  TDO     D4  X
///     A5  X       B5  MOSI    C5  TDI     D5  X
///     A6  X       B6  MUTE    C6  X       D6  X
///     A7  X       B7  SCK     C7  X       D7  AMP CONTROL
///\endverbatim
///
///   Copyright (C) 2005 David McKelvie
///
/////////////////////////////////////////////////////////////////////////////

#ifdef F_CPU
#undef F_CPU
#endif
#define F_CPU   16000000UL  // 16 MHz

#include<avr/interrupt.h>
#include<avr/pgmspace.h>
#include<avr/version.h>
#include<avr/sleep.h>
#include<avr/io.h>
#include<string.h>
#include<stdio.h>
#include<util/delay.h>
#include"ipod.h"
#include"pcf8574.h"
#include"iic.h"
#include"iiccharlcd.h"
//#include"bargraph.h"
//#include"stv5730.h"
#include"ds1807.h"
#include"dasicarpodoc.h"
#include"ui.h"

int main(void)
{
   ipod_t ipod;                                       //iPod device handle struct
   uint16_t repeat = 0;

   inithardware();
   AMP_OFF();                                          //ensure amplifier is off
   //lcd_print( LCD_LINE_ONE , __AVR_LIBC_VERSION_STRING__ );
   ui_show_welcome();

   while (1)
   {
      if (ipod_ioctl(&ipod, IPOD_IN_DOCK))
      {
         //_delay_ms( 100 );
         if (ipod_open(&ipod))                           //open ipod, fill ipod struct
         {
            AMP_ON();

            ipod_ioctl(&ipod, IPOD_GET_TIME_STATUS);         //playing? paused?
            if (ipod.status != STATUS_PLAYING)
            {
               ipod.airCommand = AIR_COMMAND_PLAY_PAUSE;
               ipod_ioctl(&ipod, IPOD_AIR_PLAYBACK);
            }
            //ipodSetFlag( &ipod );
            ipod_ioctl(&ipod, IPOD_GET_CURRENT_POSITION);      //get current position in playlist
            ipod.song.current = ~ipod.song.current;            //force display update
            ui_update_display(&ipod);

            while (ipod_ioctl(&ipod, IPOD_IN_DOCK))
            {
               switch (getButton( MODE_NON_BLOCK, 1000))
               //get user input
               {
                  case BUTTON_PLAY_PAUSE:
                     ipod.airCommand = AIR_COMMAND_PLAY_PAUSE;       //load command into ipod struct
                     ipod_ioctl(&ipod, IPOD_AIR_PLAYBACK);           //send command
                     break;

                  case BUTTON_SKIP_FORWARD:
                     ipod.airCommand = AIR_COMMAND_SKIP_FORWARD;     //load command into ipod struct
                     ipod_ioctl(&ipod, IPOD_AIR_PLAYBACK);           //send command
                     break;

                  case BUTTON_SKIP_BACKWARD:
                     ipod.airCommand = AIR_COMMAND_SKIP_BACKWARD;    //load command into ipod struct
                     ipod_ioctl(&ipod, IPOD_AIR_PLAYBACK);           //send command
                     break;

                  case BUTTON_VOLUME_UP:
                     ds_set_volume( CHANNEL_BOTH, VOLUME_UP);
                     lcd_switch_display( LCD_DISPLAY_ONE);
                     lcd_blank_line( LCD_LINE_ONE);
                     ui_show_volume();
                     while (getButton( MODE_NON_BLOCK, 1000) == BUTTON_VOLUME_UP)
                     {
                        if (++repeat == 0x003F)
                        {
                           ds_set_volume( CHANNEL_BOTH, VOLUME_UP);
                           ui_show_volume();
                           repeat = 0;
                        }
                     }
                     repeat = 0;
                     break;

                  case BUTTON_VOLUME_DOWN:
                     ds_set_volume( CHANNEL_BOTH, VOLUME_DOWN);         //decrement volume
                     lcd_switch_display( LCD_DISPLAY_ONE);
                     lcd_blank_line( LCD_LINE_ONE);
                     ui_show_volume();
                     while (getButton( MODE_NON_BLOCK, 1000) == BUTTON_VOLUME_DOWN)
                     {
                        if (++repeat == 0x003F)
                        {
                           ds_set_volume( CHANNEL_BOTH, VOLUME_DOWN);
                           ui_show_volume();
                           repeat = 0;
                        }
                     }
                     repeat = 0;
                     break;

                  case BUTTON_PLAYLIST_NEXT:
                     if (ipod.playlist.current < ipod.playlist.total)
                     {
                        ++ipod.playlist.current;
                        ipod_ioctl(&ipod, IPOD_PRESELECT_PLAYLIST);
                        ipod_ioctl(&ipod, IPOD_EXECUTE_PLAYLIST);
                     }
                     break;

                  case BUTTON_PLAYLIST_PREV:
                     if (ipod.playlist.current)
                     {
                        --ipod.playlist.current;
                        ipod_ioctl(&ipod, IPOD_PRESELECT_PLAYLIST);
                        ipod_ioctl(&ipod, IPOD_EXECUTE_PLAYLIST);
                     }
                     break;

                  default:
                     break;
               }
               ui_update_display(&ipod);
               getButton( MODE_BLOCK, 0);                  //wait for button release
            }
         }
         else
         {
            ui_show_error();
            while (ipod_ioctl(&ipod, IPOD_IN_DOCK))
               ;         //wait for iPod removal
         }
         AMP_OFF();
         ui_show_welcome();
      }
      goToSleep();
   }
}         //end main

void inithardware(void)
{
   MCUCR = 0x00;                              //sleep disable, mode idle, INT1 low, INT0 low
   GICR = 0x00;                              //disable external interrupts
   ADCSRA = 0x00;                              //disable ADC
   ACSR = _BV(ADC);                           //diable Analog Comparator

   DDRA = 0x00;                              //make PORTA input
   PORTA = 0xFF;                              //enable PORTA pullups

   DDRB = _BV(PB4) | _BV(PB5) | _BV(PB7);      //make MOSI, SCK and SS outputs, the rest inputs
   PORTB = 0xFF;                              //enable PORTB pullups

   DDRC = _BV(PC0) | _BV(PC1);               //make SCA and SCL outputs, the rest inputs
   PORTC = ~(_BV(PC0) | _BV(PC1));            //enable SCA and SCL pullups

   DDRD = _BV(PD7);                           //make AMP control output, the rest inputs
   PORTD = _BV(PD0) | _BV(PD1) | _BV(PD3);   //enable RX TX INT1 pullups

   SPCR = _BV(MSTR) | _BV(SPE) | _BV(CPOL) | _BV(CPHA);   //enable SPI, master, fosc/4;

   TWBR = 80;                              //IIC bit rate

   ui_init();
}

uint8_t getButton(uint8_t mode, uint16_t timeout)
{
   uint8_t buttons;
   if (mode == MODE_BLOCK)
      do
      {
         iic_port_read(IIC_BUTTONS, &buttons);
      } while (buttons != RAW_BUTTON_NONE);
   while (timeout--)
   {
      iic_port_read(IIC_BUTTONS, &buttons);         //get button state
      if (buttons != RAW_BUTTON_NONE)               //key is pressed?
      {
         iic_port_read(IIC_BUTTONS, &buttons);      //get debounced state
         switch (buttons)
         //get and return key
         {
            case RAW_BUTTON_NONE:                  //no button pressed
               return BUTTON_NONE;

            case RAW_BUTTON_PLAY_PAUSE:
               return BUTTON_PLAY_PAUSE;

            case RAW_BUTTON_VOLUME_UP:
               return BUTTON_VOLUME_UP;

            case RAW_BUTTON_VOLUME_DOWN:
               return BUTTON_VOLUME_DOWN;

            case RAW_BUTTON_SKIP_FORWARD:
               return BUTTON_SKIP_FORWARD;

            case RAW_BUTTON_SKIP_BACKWARD:
               return BUTTON_SKIP_BACKWARD;

            case RAW_BUTTON_MENU:
               return BUTTON_MENU;

            default:
               return BUTTON_ERROR;
         }
      }
   }
   return BUTTON_TIMEOUT;
}

uint8_t msec2ascii(char *timeString, uint32_t time)
{
   timeString[0] = '0';
   timeString[1] = '0';
   timeString[2] = ':';
   timeString[3] = '0';
   timeString[4] = '0';
   timeString[5] = '\0';
   while (time > HOUR_ONES)      //remove hours
      time -= HOUR_ONES;
   while (time > MINUTE_TENS)      //tens of minutes
   {
      time -= MINUTE_TENS;
      timeString[0]++;
   }
   while (time > MINUTE_ONES)      //minutes
   {
      time -= MINUTE_ONES;
      timeString[1]++;
   }
   while (time > SECOND_TENS)      //tens of seconds
   {
      time -= SECOND_TENS;
      timeString[3]++;
   }
   while (time > SECOND_ONES)      //seconds
   {
      time -= SECOND_ONES;
      timeString[4]++;
   }

   return 1;
}

void delay(unsigned int delayTime)
{
   while (delayTime--)
      ;
}

//
//Description:   put the CPU into POWER DOWN state, setup to be
//            woken by low level on INT1.
//
uint8_t goToSleep(void)
{
   ADCSRA = 0x00;                              //disable ADC
   ACSR = _BV(ADC);                        //diable Analog Comparator
   MCUCR = 0x00;                              //sleep disable, mode idle, INT1 low, INT0 low
   cli();
   //disable interrupts
   GICR = _BV(INT1);                        //enable INT1
   GIFR = _BV(INTF1);                        //clear INT1 flag
   set_sleep_mode(SLEEP_MODE_PWR_DOWN);
   sleep_enable()
   ;
   sei();
   sleep_cpu()
   ;
   sleep_disable()
   ;
   GICR = 0;
   return 1;
}

//empty INT1 ISR
ISR(INT1_vect)
{
   GICR = 0;      //disable external interrupts
}

//catch unhandled interrupts, halt.
ISR(__vector_default)
{
   while (1)
      PORTA++;
}
